//
//  TestBambooAppApp.swift
//  TestBambooApp
//
//  Created by user178036 on 08.04.21.
//

import SwiftUI

@main
struct TestBambooAppApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
